<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <?php
          function HighLowEqual($nouvo, $oldo) {
              if ($nouvo > $oldo) {
                  return $nouvo . " est plus grand que " . $oldo;
              }
              elseif ($nouvo < $oldo) {
                  return $nouvo . " est plus petit que " . $oldo;
              }
              else {
                  return $nouvo . " et " . $oldo . " sont identiques";
              }
          }
          $nova = HighLowEqual(42, 42);
          echo $nova;
      ?>

      <?php
          function HighLowEqual($nouvo, $oldo) {
              $resultat = "";

              if ($nouvo > $oldo) {
                  $resultat = $nouvo . " est plus grand que " . $oldo;
              }
              elseif ($nouvo < $oldo) {
                  $resultat = $nouvo . " est plus petit que " . $oldo;
              }
              else {
                  $resultat = $nouvo . " et " . $oldo . " sont identiques";
              }
              return $resultat;
          }
          $nova = HighLowEqual(42, 42);
          echo $nova;
      ?>

  </body>
</html>
