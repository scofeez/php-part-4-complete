<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <?php
          function genrage($genre, $age) {
              return 'Vous êtes ' . (($genre != 'femme') ? 'un homme' : 'une femme') . ' et vous êtes ' . (($age >= 18) ? 'Majeur' : 'Mineur');
          }
          $nova = genrage('homme', 16);
          echo $nova;
      ?>
  </body>
</html>
